@echo off
setlocal enabledelayedexpansion

@rem 末日定義
@rem lasyday_00 => 12月
set lasyday_00=31
set lastday_01=31
set lastday_02=29
set lastday_03=31
set lastday_04=30
set lastday_05=31
set lastday_06=30
set lastday_07=30
set lastday_08=31
set lastday_09=30
set lastday_10=31
set lastday_11=30

set curday=%1
call :echo_yesterday %curday%
call :echo_yesterday %curday%
call :echo_yesterday %curday%
goto :eof

:echo_yesterday
@rem バッチファイルでは08等を8進数とみなすため,Trickが必要
set /a yd_day=1%curday:~-2%-101
if %yd_day% LSS 10 set yd_day=0%yd_day%
if not %yd_day% == 00 (
   set curday=%curday:~0,6%%yd_day%
   echo !curday!
   goto :eof
)

set /a yd_mon=1%curday:~4,2%-101
if %yd_mon% LSS 10 set yd_mon=0%yd_mon%
call set yd_day=%%lastday_!yd_mon!%%
if not %yd_mon% == 00 (
   set curday=%curday:~0,4%%yd_mon%!yd_day!
   echo !curday!
   goto :eof
)

set /a yd_year=%curday:~0,4%-1
set curday=%yd_year%1231
echo %curday%
goto :eof


@rem うるう年未対応
@rem 入力値未チェック
@rem command成否未チェック
