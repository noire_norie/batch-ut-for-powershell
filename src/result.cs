﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ut
{
    public class result
    {
        private bool _result;
        public result(bool r) { _result = r; }

        public static explicit operator bool(result v) { return v.ToBoolean(); }
        public static implicit operator result(System.Boolean b) { return new result(b); }

        //
        public bool Equals(System.Boolean b) { return _result == b; }
        public bool Equals(result b) { return b.Equals(_result); }
        // powershell用のEquals定義。powershellからは基本的にobjectやobject[]で来ることが多いみたい。
        public override bool Equals(object obj) { return (obj is result) ? this.Equals((result)obj) : base.Equals(obj); }

        // powershellで結果表示にToStringが必要。
        public override string ToString() { return _result ? "True" : "False"; }
        public bool ToBoolean() { return _result; }
        public override int GetHashCode() { return base.GetHashCode(); }
    };

}
