﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

namespace ut
{
    public class notes
    {
        private class MsgLine
        {
            public enum LineType { stdout, stderr };

            private string msg;
            private LineType type;

            public MsgLine(string m, LineType t)
            {
                msg = m;
                type = t;
            }

            public void show()
            {
                Console.ForegroundColor = type == LineType.stdout ? ConsoleColor.Gray : ConsoleColor.Magenta;
                Console.WriteLine(msg);
                Console.ResetColor();
            }
        }

        private List<MsgLine> note = new List<MsgLine> { };

        public notes(Object line)
        {
            this.add(line.ToString());
        }
        public notes(Object[] lines)
        {
            foreach (var l in lines)
            {
                this.add(l);
            }
        }

        public void add(Object l)
        {
            if (l is PSObject && ((PSObject)l).TypeNames[0] == "System.Management.Automation.ErrorRecord")
            {
                note.Add(new MsgLine(l.ToString(), MsgLine.LineType.stderr));
            }
            else
            {
                note.Add(new MsgLine(l.ToString(), MsgLine.LineType.stdout));
            }
        }

        public void show()
        {
            note.ForEach(x => x.show());
        }
    }
}
