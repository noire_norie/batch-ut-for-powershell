﻿using System;
using System.Collections;
using System.Linq;
using System.Management.Automation;

namespace ut
{
    public class TestRunner
    {
        static public void Execute(string name, ScriptBlock script)
        {
            Console.Write(String.Format("Run-Test {0} ... ", name));
            var results = script.Invoke();
            var fails = results.Where(result => result.TypeNames[0] == "ut.result" && result.Equals(false));

            if (fails.Count() == 0)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success.");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(String.Format("Failed {0} test(s).", fails.Count()));

                foreach (var note in results.Where(r => r.TypeNames[0] == "ut.notes").Select(x => x.ImmediateBaseObject))
                {
                    ((notes)note).show();
                }
            }
            Console.ResetColor();
        }

        static public void Execute(Hashtable tests)
        {
            foreach(DictionaryEntry test in (new SortedList(tests)))
            {
                if (test.Value is ScriptBlock)
                {
                    Execute((string)test.Key, (ScriptBlock)test.Value);
                }
            }
        }

        static public void Execute(Hashtable tests, Object[] args)
        {
            if(args.Length == 0 )
            {
                Execute(tests);
            }
            else
            {
                foreach(string k in args)
                {
                    Execute(k, (ScriptBlock)tests[k]);
                }
            }
        }

    }
}