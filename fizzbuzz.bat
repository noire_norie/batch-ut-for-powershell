@echo off
setlocal enabledelayedexpansion
for /L %%n in (1,1,20) do (
	set /a mod3=%%n %% 3
	set /a mod5=%%n %% 5
	set /a mod15=%%n %% 15
	set disp=%%n
	if !mod3! equ 0 set disp=Fizz
	if !mod5! equ 0 set disp=Buzz
	if !mod15! equ 0 set disp=FizzBuzz
	echo !disp!
)

