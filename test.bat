@echo off

REM 遅延環境変数(!i!)の有効化
setlocal ENABLEDELAYEDEXPANSION

REM 添え字をつけて変数を定義
set VAR_1=hoge
set VAR_2=fuga
set VAR_3=foo

REM foreachループ処理
set i=1
:BEGIN
call set it=%%VAR_!i!%%
if defined it (
  echo it=!it!
  set /A i+=1
  goto :BEGIN
)
