# sample
$lib="C:\Users\tsubone\Documents\factory\batut\bin\Debug\BatchUnitTest.dll"
[void][Reflection.Assembly]::LoadFile($lib)

$tests = @{
	t00_test1 = {
		[ut.result](1 -eq 1)
		[ut.result](10 -gt 1)
	};
	t01_test2 = {
		[ut.notes]"this is a test."
		[ut.result]$false
	}
}

[ut.TestRunner]::Execute($tests, $args)


