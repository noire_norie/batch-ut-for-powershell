# test for mkyymmdd.bat
$lib="C:\Users\tsubone\Documents\factory\batut\bin\Debug\BatchUnitTest.dll"
[void][Reflection.Assembly]::LoadFile($lib)

$targbat = ".\mkyymmdd.bat"

function mknote($a, $b) {
	($a | % { "< "+$_ } ) + ($b | % { "> "+$_ })
}

$tests = @{
	t00_Oct03 = {
		$refs = @("20161002", "20161001", "20160930")
		$comp = & $targbat 20161003
		[ut.result]((Compare-Object $refs $comp) -isnot [array])
		[ut.notes](mknote $refs $comp)
	}
	t01_Sep02 = {
		$refs = @("20160901", "20160831", "20160830")
		$comp = & $targbat 20160902
		[ut.result]((Compare-Object $refs $comp) -isnot [array])
		[ut.notes](mknote $refs $comp)
	};
	t02_Aug01 = {
		$refs = @("20160731", "20160730", "20160729")
		$comp = & $targbat 20160801
		[ut.result]((Compare-Object $refs $comp) -isnot [array])
		[ut.notes](mknote $refs $comp)
	}
	t03_Jan02 = {
		$refs = @("20160101", "20151231", "20151230")
		$comp = & $targbat 20160102
		[ut.result]((Compare-Object $refs $comp) -isnot [array])
		[ut.notes](mknote $refs $comp)
	}
}

[ut.TestRunner]::Execute($tests, $args)
